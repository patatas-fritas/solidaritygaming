-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: ongames
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `imgprofile`
--

DROP TABLE IF EXISTS `imgprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `imgprofile` (
  `idImgProfile` int NOT NULL AUTO_INCREMENT,
  `Img_Profile_Src` varchar(255) NOT NULL,
  `Img_Profile_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idImgProfile`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imgprofile`
--

LOCK TABLES `imgprofile` WRITE;
/*!40000 ALTER TABLE `imgprofile` DISABLE KEYS */;
INSERT INTO `imgprofile` VALUES (1,'https://wallpaperaccess.com/full/3915806.jpg','IMG 1'),(2,'https://www.seekpng.com/png/detail/200-2009935_toilet-png-by-dianasurvive-cool-profile-pics-gaming.png','IMG 2'),(3,'https://bestprofilepictures.com/wp-content/uploads/2021/08/Gamers-Profile-Pictures.jpg','IMG 3'),(4,'https://image.shutterstock.com/image-vector/logo-esport-panda-angry-expression-260nw-1844342980.jpg','IMG 4'),(5,'https://wallpapercave.com/wp/wp9549819.jpg','IMG 5'),(6,'https://preview.redd.it/s0u1qmhwg6c21.jpg?width=640&crop=smart&auto=webp&s=a8074e4c70727fadd7a9d0e393b8b2fe5cc280bc','IMG 6'),(7,'https://i.pinimg.com/originals/fc/ee/81/fcee81535f1c868d590923fe08227845.jpg','IMG 7');
/*!40000 ALTER TABLE `imgprofile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-19 19:08:32
