const KEY_ARROW_DOWN = 0;
const KEY_ARROW_UP = 1;
const KEY_ARROW_LEFT = 2;
const KEY_ARROW_RIGHT = 3;
const KEY_SPACEBAR = 4;
const KEY_MAX_KEYS = 5;
var arca;
var width = 1600;
var height = 900;
var background_image = new Image(1600,900); ;
var eInstance;
var ben;

//percent of the size
var size=50;

//activa la vision de debugador, para ver mas facilmente lo que esta ocurriendo
var debug = false;
function ToggleDebug()
{
    if(debug)
        debug=false;
    else
        debug=true;
}

var update_score_interval = -1;
//clase que opera todo
class Game{
    constructor(background)
    {

		this.id = "";

        this.score = 0;

        console.log(background);
        if(background != null)
            background_image.src = background;
        else
            background_image = null;

        //GetCanvasElement().css("width",TransformIntoGameScreenSpace(width));
        //GetCanvasElement().css("height",TransformIntoGameScreenSpace(height));
        document.getElementById("game").width = TransformIntoGameScreenSpace(width);
        document.getElementById("game").height = TransformIntoGameScreenSpace(height);
        RootGameObject = new GameObject();
        //SetSize(width,height,RootGameObject);

        window.addEventListener("keydown", function(e) {
            if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
                e.preventDefault();
            }
        }, false);

        console.log("initialized game " + document.getElementById("game").width +" "+document.getElementById("game").height);
    };
    OnStart(){
        InitKeyBoard();

        arca = setInterval(DrawScene,16);
        console.log("xd")

        setInterval( function() { SetScoreInWebPage(game) },200);
    }
    //TO DO, QUE SE EJECUTEN LAS COSAS PERIODICAMENTE (16ms/60FPS)


    //SUPPORTED KEYS: SPACEBAR, DIRECTION KEYS

}
Keyboard = [];

function InitKeyBoard() {

    for (var i = 0; i < KEY_MAX_KEYS; ++i) {
        Keyboard.push(false);
    
    }

    document.addEventListener('keydown', function(key){

        switch (key.key) {
            case "Up":
            case "ArrowUp":
                Keyboard[KEY_ARROW_UP] = true;
                break;
            case "Down":
            case "ArrowDown":
                Keyboard[KEY_ARROW_DOWN] = true;
                break;
            case "Left":
            case "ArrowLeft":
                Keyboard[KEY_ARROW_LEFT] = true;
                break;
            case "Right":
            case "ArrowRight":
                Keyboard[KEY_ARROW_RIGHT] = true;
                break;
            case " ":
            case "Spacebar":
                Keyboard[KEY_SPACEBAR] = true;
                break;
            default:
                return;
        }

    });


        document.addEventListener('keyup', function(key){
            switch (key.key) {
                case "Up":
                case "ArrowUp":
                    Keyboard[KEY_ARROW_UP] = false;
                    break;
                case "Down":
                case "ArrowDown":
                    Keyboard[KEY_ARROW_DOWN] = false;
                    break;
                case "Left":
                case "ArrowLeft":
                    Keyboard[KEY_ARROW_LEFT] = false;
                    break;
                case "Right":
                case "ArrowRight":
                    Keyboard[KEY_ARROW_RIGHT] = false;
                    break;
                case " ":
                case "Spacebar":
                    Keyboard[KEY_SPACEBAR] = false;
                    break;
                default:
                    return;
            }
        });
}

function GetKeyState(key)
{
    return Keyboard[key];
}


function InitMouse(){
//TO DO 
document.addEventListener("click",function(evt) {
    var rect = document.getElementById("game").getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
})
}

function SetScoreInWebPage(game) {
    
    document.getElementById("scoreboard").innerHTML = game.score;

}

function GetCanvasElement()
{
    return $("canvas.game");
}


var number_of_gameobjects = 0;
var RootGameObject;

class GameObject{
    constructor(_x,_y)
    {
        this.children=[];
        this.id = GetID();
        this.x = _x;
        this.y = _y;
        this.z = 0;
        this.alpha =1.0;    }
    
    Update()
    {

        //QUEDA AQUI POR SI OS ES UTIL
        /*if(this.children != null && this.children.length>0)
        {
            this.children.forEach(child => {
                child.Update();
            });
        }*/
    };

    Draw(ctx)
    {

        var draw_x =TransformIntoGameScreenSpace(this.x);
        var draw_y = TransformIntoGameScreenSpace(this.y);
        var draw_w = TransformIntoGameScreenSpace(this.width);
        var draw_h = TransformIntoGameScreenSpace(this.height);


        if(this.sprite != null){
            ctx.globalAlpha = this.alpha;
            ctx.drawImage(this.sprite,
                draw_x,
                draw_y,
                draw_w,
                draw_h);
        }
        ctx.globalAlpha=1.0;

        if(debug){
            ctx.globalAlpha = 0.3;
            ctx.fillRect(
                draw_x,
                draw_y,
                draw_w,
                draw_h);
        }


        if(this.hasOwnProperty( 'text'))
        {
            ctx.globalAlpha = 1.0;

            if(this.hasOwnProperty('textColor'))
                ctx.fillStyle = this.textColor;

                
            if(this.hasOwnProperty("textSize"))
            {
                var new_font = Math.floor(size*this.textSize) + "px Verdana";
                ctx.font = new_font;
                //ctx.font = "30px Times New Roman"
            }
            else
            {
                ctx.font = toString(Math.floor(size)) + "px Verdana";

            }


            ctx.fillText(this.text,draw_x+draw_w/2,draw_y+draw_h/2);
            ctx.fillStyle = "#FF0000";

        }


        if(this.children != null && this.children.length>0)
        {
            this.children.forEach(child => {
                child.Draw(ctx);
            });
        }
    }

}

function ShowHierarchy()
{
    console.log(RootGameObject.children);
}

function GetID()
{
    return number_of_gameobjects++;
}

function AddGameObject(parent,_gameobject)
{
    var new_gameobject = _gameobject;
    var parentGameObj = RootGameObject;
    if(parent != null){
        parentGameObj = parent;
    }
    //console.log(parentGameObj.id);
    
    parentGameObj.children.push(new_gameobject);

    //TO DO ORDER GAME OBJECTS BY Z

    parentGameObj.children.sort(function(a,b){
        return b.z-a.z;
    })

    return new_gameobject;
}

function SetSize(_width, _height, target)
{
    target.width = _width;
    target.height = _height;
}
function SetImage(_src, target)
{
    target.sprite = new Image();
    target.sprite.src = _src;
}
function SetPosition(_x,_y,target)
{
    target.x = _x;
    target.y = _y;
}
function MoveGameObject(_x,_y,target)
{
    target.x+=_x;
    target.y+=_y;
}
function TransformIntoGameScreenSpace(amount)
{
    return amount*size/100;
}

function TransformIntoScreenSpaceGame(amount)
{
    return amount*100/size;
}

function DrawScene()
{
    //pillar contexto
    var c = document.getElementById("game");
    var ctx = c.getContext("2d");
    ctx.fillStyle = "#FF0000";
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.textAlign = "center";
    ctx.globalAlpha = 1.0;

    if(background_image != null)
        ctx.drawImage(background_image, 0,0,TransformIntoGameScreenSpace(1600),TransformIntoGameScreenSpace(900));

    RootGameObject.Draw(ctx);

}

//returns an array of 2, 0 is x and 1 is y
function GetVectorToward(x_start,y_start,x_dest,y_dest)
{
    var vector = [];
    vector[0] = (x_dest-x_start);
    vector[1] = (y_dest-y_start);
    var vec_length = VectorSize(vector[0],vector[1]);
    
    vector[0] = vector[0]/vec_length;
    vector[1] = vector[1]/vec_length;


    return vector;
}

function KeepInBounds(target)
{
    var ret = true;

    if(target.x<0){
        target.x = 0;
        ret = false;
    }

    if(target.y<0){
        target.y = 0;
        ret = false;
    }

    if(target.x > width-target.width){
        target.x = width-target.width;
        ret = false;
    }

    if(target.y > height-target.height){
        target.y = height-target.height;
        ret = false;
    }
    return ret;
}

function VectorSize(x_comp,y_comp)
{
    return Math.sqrt(Math.pow(x_comp,2) + Math.pow(y_comp,2))
}


function random(x_ran,y_ran,target)
{
    target.x = Math.floor(Math.random() * x_ran);
    target.y = Math.floor(Math.random() * y_ran);
}


function CheckCollision(obj1,obj2) {
    if (obj1.x < obj2.x + obj2.width &&
        obj1.x + obj1.width > obj2.x &&
        obj1.y < obj2.y + obj2.height &&
        obj1.height + obj1.y > obj2.y) {
         return true;// HAN CHOCADO!!
     }
     return false;
}

//DELETES GAME OBJECT FROM ROOT SCENE OBJECT
function DeleteObject(obj)
{
    const index = RootGameObject.children.indexOf(obj);

    if (index > -1) {
        console.log("deleted: " + obj.id)

        RootGameObject.children.splice(index, 1);
      }
}

function DeleteElementFromArray(el,arr)
{
    const index = arr.indexOf(el);

    if (index > -1) {
        arr.splice(index, 1);
      }
}

function CanvasEnd(game)
{
    var end = new GameObject(0,0);
    SetSize(1600,900,end)
    end.z=-10;
    var img_end = new Image(1600,900); img_end.src = "../Common/end.png";
    AddGameObject(null,end);
    end.sprite = img_end;
    end.alpha = 0.7;

    var eInstance = new EI();

    ben = new BtEnd(500,580);
    SetSize(600,200,ben);
    AddGameObject(null,ben);
    var img_pa = new Image(10,10); img_pa.src = "../Common/pa.png";
    ben.sprite = img_pa;
    ben.z=-12;
    setTimeout(clear,24);
    if (sessionStorage.getItem("logged_in") == "true") {
	    var obj={
	        user : sessionStorage.getItem("user"),
	        game : game.id,
	        score : game.score
	    }
	    console.log(obj)
	
	    var req = ServerRequest("http://localhost:8080/ongames/score",obj,'POST');
	            fetch(req,{
	            headers: {
	            'Accept': 'application/json',
	            'Content-Type': 'application/json'
	            },
	            method: 'POST'})
	}
	else
		    console.log("no entro")

}

function clear(){
    clearInterval(arca);
}

class EI
{
    constructor()
    {
        document.addEventListener('mousedown',function(evt) {
            var rect = document.getElementById("game").getBoundingClientRect();

            var x = evt.clientX - rect.left;
            var y = evt.clientY - rect.top;

            x = TransformIntoScreenSpaceGame(x);
            y = TransformIntoScreenSpaceGame(y);

            if(x > ben.x && x<ben.x+ben.width && y > ben.y && y<ben.y+ben.height){
                
                ben.Click();

            }
        })
    }

}

class EIElement extends GameObject
{
    constructor(x,y)
    {
        super(x,y);
        this.z=-1;

    }
    
}

class BtEnd extends EIElement
{
    constructor(x,y)
    {
        super(x,y);
        this.text = "";
        this.textColor = "white";
        this.textSize = 1;
        this.alpha = 0.7;
        this.z=-3;

    }
    Click(){
        location.reload();
    }
}

