var game;

$( document ).ready(function() {
    console.log( "ready!" );
    game = new serpe("background/backpark.png");
    game.OnStart();
});

var x=50;
var y=0;
var posx,posy,i=0;
var tail = [];
var dir=39;
var segmento 
var snakeObj;
var food;



var reco;


wall = false;

var img_serpe = new Image(200,200); img_serpe.src = "sprites/serpe.png";
var img_cola = new Image(200,200); img_cola.src = "sprites/cola.png";
var img_food = new Image(200,200); img_food.src = "sprites/food.png";


class serpe extends Game
{
    OnStart(){
        super.OnStart();
        reco = setInterval(move, 200); 
        
        this.id = "Snake"
        
        document.addEventListener( "keydown", doKeyDown, true);

        var logic = new GameObject(100,100);
        AddGameObject(null,logic);
        SetSize(0,0,logic);

        snakeObj = new SnakeObj(200,200);
        AddGameObject(null,snakeObj);
        SetSize(70,70,snakeObj);
        snakeObj.sprite = img_serpe;
    
            
        segmento = new GameObject(0,0);
        AddGameObject(null,segmento);

        tail.push(new GameObject(130,200));
        tail.push(new GameObject(130,200));
        tail.push(new GameObject(60,200));

        tail.forEach(element => {
            SetSize(70,70,element);
            element.sprite = img_cola;
            AddGameObject(segmento,element);
            
        });

        console.log(tail[0].x);
        SetSize(0,0,tail[0]);

        food = new Food(0,0);
        random(1530,830,food);
        AddGameObject(null,food);
        SetSize(70,70,food);
        food.sprite = img_food;
        
    }    
}

class SnakeObj extends GameObject
{
    

}
class Score extends GameObject
{
    constructor(x,y)
    {
        super(x,y);
        this.text = "Score:";
        this.textColor = "Black";
        this.textSize = 0.5;
        
    }
    
}
 
class Food extends GameObject
{
    
}


function move()
{    
    if(snakeObj.x<0||snakeObj.x>1599||snakeObj.y<0||snakeObj.y>899){
           
        wall = true;     
        CanvasEnd(game);      
        clearInterval(reco);
    }

    if(wall==false){
        posx = snakeObj.x;
        posy = snakeObj.y;
        MoveGameObject(x,y,snakeObj);
        SetPosition(posx,posy,tail[0]);
        for(m=tail.length-1;m>0;m--){ 
            if(snakeObj.x==tail[m].x&&snakeObj.y==tail[m].y){
                wall =true;
                CanvasEnd(game);
                clearInterval(reco);
            }
            posx = tail[m-1].x;
            posy = tail[m-1].y;
            SetPosition(posx,posy,tail[m]);
            
        }
        
    }  

        if(CheckCollision(food,snakeObj)){
            random(1530,830,food);            
            var f = 0;
            game.score += 50;
           
           
            for(f=0;f<tail.lenght-1;f++){
                if(food.x==tail[f].x&&food.y==tail[f].y){
                    random(1530,830,food);
                    f=0;
                }
            }
            cola();
            return false;
        
    }  
}

function doKeyDown(e)
{
    var k =e.keyCode;
    
    if(dir!=39&&k==37){
        dir=37;
        x=-70;
        y=0;
    }
    if(dir!=40&&k==38){
        dir = 38;
        x=0;
        y=-70;
    }
    if(dir!=37&&k==39){
        dir=39;
        x=+70;
        y=0;
    }
    if(k==40&&dir!=38){
        dir=40;
        x=0;
        y=+70;
    }
}

function cola()
{
    var total = tail.length-1;
    posx = tail[total].x;
    posy = tail[total].y;

    var new_tail_length = tail.push(new GameObject(posx+70,posy));    
    SetSize(70,70,tail[new_tail_length-1]);
    tail[new_tail_length-1].sprite = img_cola;
    AddGameObject(null, tail[new_tail_length-1]);       
}

