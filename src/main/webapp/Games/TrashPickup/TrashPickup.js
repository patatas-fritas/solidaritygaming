var game;

$( document ).ready(function() {
    console.log( "ready!" );
    game = new TrashPickup("background/background.png");
    game.OnStart();
});
var points_parent;
var points = [];

var towers = [];
var current_tower_selected;

var enemy_controller;
var tower_controller;

var money = 100;
var health = 10;


var uiInstance;

const StartingEnemyPos = {x : 200, y : 900}

/*var img_turret=[
"resources/sprites/turretlvl1.png", 
"resources/sprites/turretlvl2.png", 
"resources/sprites/turretlvl3.png",
"resources/sprites/turretlvl4.png",
"resources/sprites/turretlvl5.png",
"resources/sprites/turretlvl6.png"]*/

var img_turret_1 = new Image(200,200); img_turret_1.src = "sprites/turretlvl1.png";
var img_turret_2 = new Image(200,200); img_turret_2.src = "sprites/turretlvl2.png";
var img_turret_3 = new Image(200,200); img_turret_3.src = "sprites/turretlvl3.png";
var img_turret_4 = new Image(200,200); img_turret_4.src = "sprites/turretlvl4.png";
var img_turret_5 = new Image(200,200); img_turret_5.src = "sprites/turretlvl5.png";
var img_turret_6 = new Image(200,200); img_turret_6.src = "sprites/turretlvl6.png";

var projectile_img = new Image(50,50); projectile_img.src = "sprites/projectile.png";

var img_enemy_lvl3 = new Image(400,400); img_enemy_lvl3.src = "sprites/bin_bag.png";
var img_enemy_lvl2 = new Image(400,400); img_enemy_lvl2.src = "sprites/wasted_can.png";
var img_enemy_lvl1 = new Image(400,400); img_enemy_lvl1.src = "sprites/paper_ball.png";

var enemy_controller_interval;
var tower_controller_interval;


class TrashPickup extends Game{
    constructor(background)
    {
        super(background);
    }
    OnStart()
    {

        this.id = 'TrashPickup';
        super.OnStart();

        points_parent = new GameObject(0,0);
        AddGameObject(null, points_parent);

        points.push(new GameObject(200,700));
        points.push(new GameObject(200,300));
        points.push(new GameObject(575,300));
        points.push(new GameObject(575,700));
        points.push(new GameObject(950,700));
        points.push(new GameObject(950,-50));

        points.forEach(element => {
            SetSize(50,50,element);
            AddGameObject(points_parent,element);
        });

        //in order of progression
        towers.push(new Tower(400,650));
        towers.push(new Tower(100,70));
        towers.push(new Tower(740,150));
        towers.push(new Tower(400,400));
        towers.push(new Tower(750,450));
        towers.push(new Tower(1050,180));
        towers.push(new Tower(1100,600));


        towers.forEach(element => {
            SetSize(80,80,element);
            AddGameObject(null,element);
        });



        enemy_controller = new EnemyController();
        enemy_controller_interval =setInterval(function(){enemy_controller.Update()},16);

        tower_controller = new TowerController();
        tower_controller_interval = setInterval(function(){tower_controller.Update()},16);




        var uiInstance = new UI();

        var butt1 = new Button_Build(1250,150);
        SetSize(350,200,butt1);
        AddGameObject(null,butt1);
        buttons.push(butt1);

        var butt2 = new Button_Upgrade(1250,360);
        SetSize(350,200,butt2);
        AddGameObject(null,butt2);
        buttons.push(butt2);

        var butt3 = new Button_Demolish(1250,570);
        SetSize(350,200,butt3);
        AddGameObject(null,butt3);
        buttons.push(butt3);


        var money_display = new Display_money(1400,0);
        SetSize(1,100,money_display);
        AddGameObject(null,money_display);
        displays.push(money_display);

        var health_display= new Display_health(1400,40);
        SetSize(1,100,health_display);
        AddGameObject(null,health_display);
        displays.push(health_display);

        var round_display = new Display_round(1400,80);
        SetSize(1,100,round_display);
        AddGameObject(null,round_display);
        displays.push(round_display);

    }


    EndGame(){
        CanvasEnd(game);
        clearInterval(enemy_controller_interval);
        clearInterval(tower_controller_interval);
        clearInterval(UIupdateInterval);
        towers.forEach(element => {
            clearInterval(element.interval);
        });
    }
}
var close_enough=5;



class Tower extends GameObject{
    constructor(x,y)
    {
        super(x,y);
        this.damage = 0;
        this.level = 0;
        this.radius = 300;
        this.target = null;
        this.interval = null;
    }

    Upgrade()
    {
        this.level+=1;
        
        var attr = GetTowerLevelAttribute(this.level);
        this.sprite = attr.img;
        this.damage = attr.damage;

        console.log("Upgraded to level "+this.level);

        //change attributes
    }

    Destroy()
    {
        var attr = GetTowerLevelAttribute(this.level);
        this.level = 0;
        money+=attr.sell;
        attr = GetTowerLevelAttribute(this.level);
        this.sprite = attr.img;
        this.damage = attr.damage;

    }

    Shoot()
    {
        
        console.log("calling shoot: " +this.id )
        if(this.target!=null && VectorSize((this.target.x+this.target.width/2)-(this.x+this.width/2), (this.target.y+this.target.height/2)-(this.y+this.height/2)) < this.radius)
        {
            var direction = GetVectorToward(this.x+this.width/2,this.y+this.height/2,(this.target.x+this.target.width/2),(this.target.y+this.target.height/2));

            var pro = new Projectile(this.x+this.width/2,this.y+this.height/2,10,this.damage,projectile_img);
            pro.speed_x = direction[0]*20;
            pro.speed_y = direction[1]*20;
            projectiles.push(pro);
            SetSize(20,20,pro);
            AddGameObject(null,pro);
        }
        else{
            clearInterval(this.interval);
            this.interval = null;
            this.target = null;
        }
    }
}

var projectiles = [];


class Projectile extends GameObject{
    constructor(x,y,_speed,_damage,_img)
    {
        super(x,y);

        this.speed = _speed;
        this.damage = _damage;
        this.sprite = _img;
    }

    
}



class TowerController{
    constructor()
    {
        document.addEventListener('mousedown',function(evt) {
            var rect = document.getElementById("game").getBoundingClientRect();

            var x = evt.clientX - rect.left;
            var y = evt.clientY - rect.top;

            x = TransformIntoScreenSpaceGame(x);
            y = TransformIntoScreenSpaceGame(y);

            towers.forEach(tower =>{
                if(x > tower.x && x<tower.x+tower.width && y > tower.y && y<tower.y+tower.height)
                {
                    current_tower_selected = tower;
                    buttons.forEach(element => {
                        element.DisplayCost()
                    });
                    console.log("tower selected");
                }

            }) 

        })
    }
    Update(){
        towers.forEach(tower => {
            if(tower.damage>0){
            
            //shoot/play animation
            if(tower.target == null)
            {
                enemies.forEach(enemy => {
                    if(VectorSize(enemy.x-tower.x,enemy.y-tower.y) < tower.radius)
                    {
                        tower.target = enemy;
                        console.log("found target: "+ enemy.id)
                    }
                });
                clearInterval(tower.interval);
                tower.interval = null;
            }
            if(tower.target != null)
            {
                //shoot
                if(tower.interval == null)
                {

                    tower.interval = setInterval(function(){tower.Shoot()},1000);
                }
            }

            }
        })

        projectiles.forEach(projectile => {

            projectile.x+=projectile.speed_x;
            projectile.y+=projectile.speed_y;
        });
    }

    UpgradeTower(){

    }
}

class Enemy extends GameObject{
    constructor(x,y)
    {
        super(x,y);
        this.last_visited_point=0;

        this.speed = 2;
        this.health = 5;

    }
}

var enemies = [];

var lvl1s = 0;
var lvl2s = 0;
var lvl3s = 0;

var iterator=0;
var lvl1Spawned = 0;
var lvl2Spawned = 0;
var lvl3Spawned = 0;
var spawn_interval = null;

function SpawnEnemy()
{
    var fail_1, fail_2, fail_3 = false;
    while (!fail_1 || !fail_2 || !fail_3) {

        var number = Math.floor(Math.random() * 3) + 1;
        switch (number) {
            case 1:
                    if(lvl1Spawned >=lvl1s ){
                        fail_1=true;
                    }
                    else
                    {
                        //spawn enemy 1
                        var new_enemy = new Enemy(StartingEnemyPos.x,StartingEnemyPos.y);
                        SetSize(60,60,new_enemy);
                        AddGameObject(null,new_enemy);
                        enemies.push(new_enemy);
                        iterator++;
                        lvl1Spawned++;
                        new_enemy.sprite = img_enemy_lvl1;
                        return new_enemy;
                    }
                break;
            case 2:
                if(lvl2Spawned >=lvl2s ){
                    fail_2=true;
                }
                else
                {
                    //spawn enemy 2
                    var new_enemy = new Enemy(StartingEnemyPos.x,StartingEnemyPos.y);
                    SetSize(80,80,new_enemy);
                    AddGameObject(null,new_enemy);
                    enemies.push(new_enemy);
                    new_enemy.health=10;
                    iterator++;
                    lvl2Spawned++;
                    new_enemy.sprite = img_enemy_lvl2;
                    return new_enemy;
                }
                break;
            case 3:
                if(lvl3Spawned >=lvl3s ){
                    fail_3=true;
                }
                else
                {
                    //spawn enemy 3
                    var new_enemy = new Enemy(StartingEnemyPos.x,StartingEnemyPos.y);
                    SetSize(100,100,new_enemy);
                    AddGameObject(null,new_enemy);
                    enemies.push(new_enemy);
                    new_enemy.health=20;
                    iterator++;
                    lvl3Spawned++;
                    new_enemy.sprite = img_enemy_lvl3;
                    return new_enemy;
                }
                break;
                break;
            default:
                break;
        }
    }
    //this means that it couldn't spawn anyting, which means the function is done
    clearInterval(spawn_interval);
    lvl1Spawned = 0;
    lvl2Spawned = 0;
    lvl3Spawned = 0;

}

class EnemyController{

    constructor()
    {
        this.round = 0;
        this.NextRound();
    }

    NextRound()
    {
        console.log("starting round");
        iterator=0;
        this.round+=1;
        lvl1s+=1;
        if(this.round % 2 === 0)
        {
            lvl1s+=1;
        }
        if(this.round % 3 === 0)
        {
            lvl1s-=1;
        }
        if(this.round % 5 === 0)
        {
            lvl2s+=1;
        }
        if(this.round % 10 === 0)
        {
            lvl3s+=1;
        }

        console.log("spawning " + lvl1s +" lvl1s, "+lvl2s + " lvl2s, "+lvl3s + "lvl3s" )

        var interval_number =1000-this.round*30;
        if(interval_number < 20)
        {interval_number =20;}
        spawn_interval = setInterval(SpawnEnemy,interval_number);
    }

    DeleteEnemy(enemy)
    {
        DeleteObject(enemy);
        DeleteElementFromArray(enemy,enemies);

        if(enemies.length <=0)
        {
            setTimeout(function(){enemy_controller.NextRound()},10000)
        }
    }

    Update(){
        enemies.forEach(enemy => {
            var going_to = points[enemy.last_visited_point];
            var vect = GetVectorToward(enemy.x,enemy.y,going_to.x,going_to.y);
    
            //console.log(VectorSize(vect[0],vect[1]));
    
            enemy.x+=vect[0]*enemy.speed;
            enemy.y+=vect[1]*enemy.speed;
    
            if(VectorSize(going_to.x-enemy.x,going_to.y-enemy.y)<close_enough)
            {
                console.log("visited"+enemy.last_visited_point);
                enemy.last_visited_point++;
                if(points.length-1<enemy.last_visited_point)
                {
                    enemy.last_visited_point = points.length-1;
                    health-=1;

                    this.DeleteEnemy(enemy);
                    if(health==0)
                    {
                        //GAME OVER
                        game.EndGame();
                    }


                }
            }

            projectiles.forEach(pr => {
                if(CheckCollision(pr,enemy))
                {
                    DeleteElementFromArray(pr,projectiles);
                    enemy.health-=pr.damage;
                    DeleteObject(pr);

                    if(enemy.health <= 0)
                    {
                        this.DeleteEnemy(enemy);
                        
                        switch (enemy.sprite) {
                            case img_enemy_lvl1:
                                money+=30;
                                game.score+=10;
                                break;
                            case img_enemy_lvl2:
                                money+=70;
                                game.score+=30;
                                break;
                            case img_enemy_lvl3:
                                money+=150;
                                game.score+=50;
                                break;
                            default:
                                break;
                        }

                        towers.forEach(tower => {
                            if(tower.target == enemy)
                            {
                                tower.target =null;
                            }
                        });


                    }
                }
            });
    
        });
    }
}



var buttons = [];
var displays = [];
var UIupdateInterval;
class UI{

    constructor()
    {
        UIupdateInterval = setInterval(UpdateUI,100);

        document.addEventListener('mousedown',function(evt) {
            var rect = document.getElementById("game").getBoundingClientRect();

            var x = evt.clientX - rect.left;
            var y = evt.clientY - rect.top;

            x = TransformIntoScreenSpaceGame(x);
            y = TransformIntoScreenSpaceGame(y);

            buttons.forEach(button => {
                if(x > button.x && x<button.x+button.width && y > button.y && y<button.y+button.height)
                    button.Click();
            }) 

        })
    }

}

function UpdateUI()
{
    displays.forEach(element => {
        element.Update();
        //console.log(element);
    });
}

class UIElement extends GameObject{
    constructor(x,y)
    {
        super(x,y);
        this.z=-1;

    }
    
}

class Button_Build extends UIElement{
    constructor(x,y)
    {
        super(x,y);
        this.text = "Build";
        this.textColor = "black";
        this.textSize = 0.5;
        

    }
    Click(){
        console.log("clicked build button")
        var attr = GetTowerLevelAttribute(1);
        if(current_tower_selected.level == 0 && money >= attr.cost)
        {
            money -=attr.cost;
            current_tower_selected.Upgrade();

            buttons.forEach(element => {
                element.DisplayCost()
            });
        }
    }
    DisplayCost()
    {
        var attr = GetTowerLevelAttribute(1);
        this.text = "Build("+attr.cost+")"
    }
}

class Button_Upgrade extends UIElement{
    constructor(x,y)
    {
        super(x,y);
        this.text = "Upgrade";
        this.textColor = "black";
        this.textSize = 0.5;
    }
    Click(){
        console.log("clicked Upgrade button")
        var attr = GetTowerLevelAttribute(current_tower_selected.level+1);
        if(current_tower_selected.level < 6 && current_tower_selected.level != 0 && money >= attr.cost)
        {
            money-=attr.cost;
            current_tower_selected.Upgrade();

            buttons.forEach(element => {
                element.DisplayCost()
            });
        }
    }
    DisplayCost()
    {
        var attr = GetTowerLevelAttribute(current_tower_selected.level+1);
        this.text = "Upgrade("+attr.cost+")"
    }
}


class Button_Demolish extends UIElement{
    constructor(x,y)
    {
        super(x,y);
        this.text = "Demolish";
        this.textColor = "black";
        this.textSize = 0.5;
    }
    Click(){
        current_tower_selected.Destroy();
        buttons.forEach(element => {
            element.DisplayCost()
        });
    }
    DisplayCost()
    {
        var attr = GetTowerLevelAttribute(current_tower_selected.level);
        this.text = "Demolish(" + attr.sell + ")"
    }
}

class Display_money extends UIElement{
    constructor(x,y)
    {
        super(x,y);
        this.text = "Money";
        this.textColor = "black";
        this.textSize = 0.4;
    }
    Update(){
        this.text = "Money: "+ money;
    }
}
class Display_health extends UIElement{
    constructor(x,y)
    {
        super(x,y);
        this.text = "Health";
        this.textColor = "black";
        this.textSize = 0.4;
    }
    Update(){
        this.text = "Health: "+ health;
    }
}
class Display_round extends UIElement{
    constructor(x,y)
    {
        super(x,y);
        this.text = "Round";
        this.textColor = "black";
        this.textSize = 0.4;
    }
    Update(){
        this.text = "Round: "+ enemy_controller.round;
    }
}

function GetTowerLevelAttribute(level)
{
    switch (level) {
        case 0:
            return{
                damage:0,
                cost:0,
                img:null,
                sell:0
            }
            break;
        case 1:
            return{
                damage : 2,
                cost : 100,
                img : img_turret_1,
                sell : 50
            }
            break;
        case 2:
            return{
                damage : 4,
                cost : 100,
                img : img_turret_2,
                sell:100
            }
            break;
        case 3:
            return{
                damage : 8,
                cost : 200,
                img : img_turret_3,
                sell:150
            }
            break;
        case 4:
            return{
                damage : 12,
                cost : 500,
                img : img_turret_4,
                sell:200
            }
            break;
        case 5:
            return{
                damage : 15,
                cost : 750,
                img : img_turret_5,
                sell:400
            }
            break;
        case 6:
            return{
                damage : 20,
                cost : 1000,
                img : img_turret_6,
                sell : 500
            }
            break;
            case 7:
                return{
                    cost:"max",
                    sell:500
            }
            break;
        default:
            break;
    }
}

