var game;
$(document).ready(function () {
    console.log("ready!");
    game = new CocheCiclistas(null);
    game.OnStart();
});

var wallpaper, wallpaper2, peloton, coche, forest, bosque, bosque2, bosque3, bosque4, choque = false, timing1, timing2, timing3, timing4, contador = 0;

var update_interval = -1;

class CocheCiclistas extends Game {
    OnStart() {
	    super.OnStart();

		this.id = "Coche esquiva"
	
        wallpaper = new Fondo(300, 0);
        AddGameObject(null, wallpaper);
        SetSize(1000, 900, wallpaper);
        SetImage("background/carretera.png", wallpaper)

        wallpaper2 = new Fondo(300, -900);
        AddGameObject(null, wallpaper2);
        SetSize(1000, 900, wallpaper2);
        SetImage("background/carretera.png", wallpaper2)

        bosque = new Bosque(0, 0);
        AddGameObject(forest, bosque);
        SetSize(300, 900, bosque);
        SetImage("background/bosque.png", bosque)

        bosque2 = new Bosque(0, -900);
        AddGameObject(forest, bosque2);
        SetSize(300, 900, bosque2);
        SetImage("background/bosque.png", bosque2)

        bosque3 = new Bosque(1300, 0);
        AddGameObject(forest, bosque3);
        SetSize(300, 900, bosque3);
        SetImage("background/bosque2.png", bosque3)

        bosque4 = new Bosque(1300, -900);
        AddGameObject(forest, bosque4);
        SetSize(300, 900, bosque4);
        SetImage("background/bosque2.png", bosque4)

        forest = new GameObject(0, 0);
        AddGameObject(null, forest);

        peloton = new GameObject(0, 0);
        AddGameObject(null, peloton)

        coche = new Coche(750, 700);
        AddGameObject(null, coche);
        SetSize(80, 162, coche);
        SetImage("sprites/coche_rosa_recorte.jpg", coche)

        // RandomAxisPosition();
        // var bici = new Bibicleta(rndmx, rndmy)
        // AddGameObject(peloton , bici);
        // SetSize(35, 104, bici);
        // SetImage("Img/CocheCiclistas/bici1.png", bici);

        // RandomAxisPosition();
        // var bici = new Bibicleta(rndmx, rndmystatic)
        // AddGameObject(peloton, bici);
        // SetSize(35, 104, bici);
        // SetImage("Img/CocheCiclistas/bici1.png", bici);

        //ForestIzquierda();
        //ForestDerecha();

        timing1 = setTimeout(Peloton1Creator, RandomTime());
        timing2 = setTimeout(Peloton2Creator, RandomTime());
        timing3 = setTimeout(Peloton3Creator, RandomTime());
        timing4 = setTimeout(Peloton4Creator, RandomTime());
        update_interval = setInterval(UpdateScene, 16);

    }
}

function UpdateScene() {
    wallpaper.Update();
    if ((wallpaper.y >= 900) && (wallpaper.y <= (907 + contador))) {
        wallpaper.y = -900;
    }
    wallpaper2.Update();
    if ((wallpaper2.y >= 900) && (wallpaper2.y <= (907 + contador))) {
        wallpaper2.y = -900;
    }
    peloton.children.forEach(bici => {
        bici.Update();
        if ((bici.y >= 900) && (bici.y <= (905 + contador))){
            game.score += 10;
        }
    })
    coche.Update();
    bosque.Update();
    if ((bosque.y >= 900) && (bosque.y <= (907 + contador))) {
        bosque.y = -900;
    }
    bosque2.Update();
    if ((bosque2.y >= 900) && (bosque2.y <= (907 + contador))) {
        bosque2.y = -900;
    }
    bosque3.Update();
    if ((bosque3.y >= 900) && (bosque3.y <= (907 + contador))) {
        bosque3.y = -900;
    }
    bosque4.Update();
    if ((bosque4.y >= 900) && (bosque4.y <= (907 + contador))) {
        bosque4.y = -900;
    }
    //console.log(game.score);
}

class Fondo extends GameObject {
    Update () {
        this.y = this.y + 7 + contador;
    }
}

class Coche extends GameObject {
    Update() {
        peloton.children.forEach(bici => {
            if (CheckCollision(this, bici)) {
                //console.log("choque!")
                choque = true;
                if (choque == true) {// aquest if no cal, es podria escriure el q hi ha dins sense l'if i funcionaria igual
                    CanvasEnd(game);
                    timing1 = 0;
                    timing2 = 0;
                    timing3 = 0;
                    timing4 = 0;
                    clearInterval(update_interval);
                }
            }
        });
        if (GetKeyState(KEY_ARROW_LEFT)) {
            this.x -= 6.5
        }
        if (GetKeyState(KEY_ARROW_RIGHT)) {
            this.x += 6.5
        }
        if (GetKeyState(KEY_ARROW_DOWN)) {
            this.y += 6.5;
        }
        if (GetKeyState(KEY_ARROW_UP)) {
            this.y -= 6.5;
        }
    }
}

class Bibicleta extends GameObject {
    Update() {
        if (CheckCollision(this, coche) != true) {
            //console.log("choque!")
            this.y = this.y + 5 + contador;
        }
    }
}

class Bosque extends GameObject {
    Update() {
        if (CheckCollision(this, coche) != true) {
            this.y = this.y + 7 + contador;
        } else {
            choque = true;
                if (choque == true) {// aquest if no cal, es podria escriure el q hi ha dins sense l'if i funcionaria igual
                    CanvasEnd(game);
                    timing1 = 0;
                    timing2 = 0;
                    timing3 = 0;
                    timing4 = 0;
                    clearInterval(update_interval);
                }
        }
    }
}

/*function ForestIzquierda() { // genera objectes bosc esquerra
    bosque = new Bosque(0, -900);
    AddGameObject(forest, bosque);
    SetSize(300, 900, bosque);
    SetImage("background/bosque.png", bosque)
    generarBosque = false
    return bosque
}

function ForestDerecha() { // genera objectes bosc esquerra
    bosque2 = new Bosque(1300, -900);
    AddGameObject(forest, bosque2);
    SetSize(300, 900, bosque2);
    SetImage("background/bosque2.png", bosque2)
    generarBosque = false
    return bosque2
}*/

function RandomTime() { // genera un numero random entre 0 i 6 inclosos
    let rndmTime = Math.floor(Math.random() * (8000 - 3000)) + 3000;
    console.log("tiempo " + rndmTime)
    return rndmTime
}

var rndmx, rndmy;
var rndmystatic = -500;

function RandomAxisPosition() {
    rndmx = Math.floor(Math.random() * 1601);
    //console.log("rdnmx = "+ rndmx);
    rndmy = Math.floor(Math.random() * 901);
    //console.log("rdnmy = "+ rndmy);
}

function RandomX(min, max) { //genera random X entre max i min
    let rndmx1 = Math.floor(Math.random() * (max - min)) + min;
    return rndmx1
}

function RandomSix() { // genera un numero random entre 0 i 6 inclosos
    let rndmSix = Math.floor(Math.random() * 7);
    return rndmSix
}

function RandomTwoHundred() { // genera un numero random entre 0 i 100 inclosos
    let rndmSix = Math.floor(Math.random() * 201);
    return rndmSix
}

function RandomTrue() { // genera true o false
    let TrueorFalse;
    let numTrueorFalse = Math.floor(Math.random() * 2);
    if (numTrueorFalse == 0) {
        TrueorFalse = true;
    } else {
        TrueorFalse = false;
    }
    return TrueorFalse
}

function Ciclista() { // genera objectes bici
    var bici = new Bibicleta(0, rndmystatic)
    AddGameObject(peloton, bici);
    SetSize(35, 104, bici);
    SetImage("sprites/bici1.PNG", bici);
    return bici
}

let pelotonCarril = [], pelotonCarril1 = [], pelotonCarril2 = [], pelotonCarril3 = [], pelotonCarril4 = [];

function Peloton1Creator() { // genera X ciclistes pel carril 1
    if (choque == false) {
        let numPeloton = RandomSix();
        console.log("1 num peloton " + numPeloton)
        for (let i = 0; i < numPeloton; i++) {
            let bici = Ciclista();
            let y;
            let voltes = RandomSix() + 1, TrueorFalse = RandomTrue();
            console.log("1 voltes " + voltes)
            for (let j = 0; j < voltes; j++) {
                if (TrueorFalse == true) {
                    y = rndmystatic + RandomTwoHundred();
                    SetPosition(RandomX(340, 490), y, bici);
                    console.log("1 suma voltes " + voltes + " j " + j)
                } else {
                    y = rndmystatic - RandomTwoHundred();
                    SetPosition(RandomX(340, 490), y, bici);
                    console.log("1 resta voltes " + voltes + " j " + j)
                }
            }
            pelotonCarril1.push(bici);
            pelotonCarril.push(pelotonCarril1);
        }
        timing1 = setTimeout(Peloton1Creator, RandomTime());
        contador += 0.5;
    } else {
        console.log("choque, juego terminado")
    }
}

function Peloton2Creator() { // genera X ciclistes pel carril 2
    if (choque == false) {
        let numPeloton = RandomSix();
        console.log("2 num peloton " + numPeloton)
        for (let i = 0; i < numPeloton; i++) {
            let bici = Ciclista();
            let y;
            let voltes = RandomSix() + 1, TrueorFalse = RandomTrue();
            console.log("2 voltes " + voltes)
            for (let j = 0; j < voltes; j++) {
                if (TrueorFalse == true) {
                    y = rndmystatic + RandomTwoHundred();
                    SetPosition(RandomX(520, 760), y, bici);
                    console.log("2 suma voltes " + voltes + " j " + j)
                } else {
                    y = rndmystatic - RandomTwoHundred();
                    SetPosition(RandomX(520, 760), y, bici);
                    console.log("2 resta voltes " + voltes + " j " + j)
                }
            }
            pelotonCarril2.push(bici);
            pelotonCarril.push(pelotonCarril2);
        }
        timing2 = setTimeout(Peloton2Creator, RandomTime());
    } else {
        console.log("choque, juego terminado")
    }
}

function Peloton3Creator() { // genera X ciclistes pel carril 3
    if (choque == false) {
        let numPeloton = RandomSix();
        console.log("3 num peloton " + numPeloton)
        for (let i = 0; i < numPeloton; i++) {
            let bici = Ciclista();
            let y;
            let voltes = RandomSix() + 1, TrueorFalse = RandomTrue();
            console.log("3 voltes " + voltes)
            for (let j = 0; j < voltes; j++) {
                if (TrueorFalse == true) {
                    y = rndmystatic + RandomTwoHundred();
                    SetPosition(RandomX(790, 1000), y, bici);
                    console.log("3 suma voltes " + voltes + " j " + j)
                } else {
                    y = rndmystatic - RandomTwoHundred();
                    SetPosition(RandomX(790, 1000), y, bici);
                    console.log("3 resta voltes " + voltes + " j " + j)
                }
            }
            pelotonCarril3.push(bici);
            pelotonCarril.push(pelotonCarril3);
        }
        timing3 = setTimeout(Peloton3Creator, RandomTime());
    } else {
        console.log("choque, juego terminado")
    }
}

function Peloton4Creator() { // genera X ciclistes pel carril 4
    if (choque == false) {
        let numPeloton = RandomSix();
        console.log("4 num peloton " + numPeloton)
        for (let i = 0; i < numPeloton; i++) {
            let bici = Ciclista();
            let y;
            let voltes = RandomSix() + 1, TrueorFalse = RandomTrue();
            console.log("4 voltes " + voltes)
            for (let j = 0; j < voltes; j++) {
                if (TrueorFalse == true) {
                    y = rndmystatic + RandomTwoHundred();
                    SetPosition(RandomX(1060, 1220), y, bici);
                    console.log("4 suma voltes " + voltes + " j " + j)
                } else {
                    y = rndmystatic - RandomTwoHundred();
                    SetPosition(RandomX(1060, 1220), y, bici);
                    console.log("4 resta voltes " + voltes + " j " + j)
                }
            }
            pelotonCarril4.push(bici);
            pelotonCarril.push(pelotonCarril4);
        }
        timing4 = setTimeout(Peloton4Creator, RandomTime());
    } else {
        console.log("choque, juego terminado")
    }
}