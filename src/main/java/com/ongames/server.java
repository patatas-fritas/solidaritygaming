package com.ongames;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class server extends HttpServlet {
private static final long serialVersionUID = 1L;

String user;
String userName, userNickname, userPassword, userMail;
int idUserImg;

@Override
protected void doGet(
  HttpServletRequest request, 
  HttpServletResponse response) throws IOException {
    
      user = request.getParameter("user");
    
      Connection connection = null;
      Statement selectStmt = null;
      
      try{
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ongames", "root", "A2s6wlYFz0Rt7C7Z");
        selectStmt = connection.createStatement();
        ResultSet rs = selectStmt.executeQuery("SELECT * FROM ongames.users where User_nickname = '" + user + "';");
        
        while (rs.next()) {
            userName = rs.getString("User_name");
            userNickname = rs.getString("User_nickname");
            userPassword = rs.getString("User_password");
            userMail = rs.getString("User_mail");
            idUserImg = rs.getInt("ImgProfile_idImgProfile");
        }
      } 
      catch (Exception e) {
        e.printStackTrace();
      }finally {
        try {
          selectStmt.close();
          connection.close();
        } catch (Exception e) {
          e.printStackTrace();
        }}
   
    PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print("{");
    out.print("\"name\":\""+userName+"\"");    
    out.print(",");
    out.print("\"nickname\":\""+userNickname+"\"");
    out.print(",");
    out.print("\"password\":\""+userPassword+"\"");
    out.print(",");
    out.print("\"mail\":\""+userMail+"\"");
    out.print(",");
    out.print("\"idImg\":\""+idUserImg+"\"");
    out.print("}");
    out.flush();  
  }


@Override
protected void doPost(
  HttpServletRequest request,
  HttpServletResponse response) throws IOException {
 
    userName = request.getParameter("name");
    userNickname = request.getParameter("user");
    userPassword = request.getParameter("password");
    userMail = request.getParameter("mail");
    
    Connection connection = null;
    Statement selectStmt = null;
    
    try{
      Class.forName("com.mysql.cj.jdbc.Driver");
      connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ongames", "root", "A2s6wlYFz0Rt7C7Z");
      selectStmt = connection.createStatement();
      selectStmt.executeUpdate("INSERT INTO ongames.users (User_name, User_nickname, User_password, User_mail, ImgProfile_idImgProfile) "
      		+ "VALUES ('" + userName + "', '" + userNickname + "', '" + userPassword + "', '" + userMail + "', 1)");
    } 
      catch (Exception e) {
        e.printStackTrace();
      }finally {
        try {
          selectStmt.close();
          connection.close();
        } catch (Exception e) {
          e.printStackTrace();
        }}

  }
       
}