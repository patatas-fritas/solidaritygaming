package com.ongames;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class ScoreServlet extends HttpServlet {
private static final long serialVersionUID = 1L;

private Gson gson = new Gson();
List<String> scoresArrayList = new ArrayList<>();
String userNickname, gameName, parsScoreRequested;
int scoreRequested, idUser, idGame;



@Override
protected void doGet(
  HttpServletRequest request,
  HttpServletResponse response) throws IOException {

  String user = request.getParameter("user");
  String game = request.getParameter("game");
  System.out.println(user+" : "+game);      
    
 
      Connection connection = null;
      scoresArrayList.clear();
      Statement selectStmt = null;

      try{
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ongames", "root", "A2s6wlYFz0Rt7C7Z");
        selectStmt = connection.createStatement();
        if(user == null) {// SQL que devuelve Scores del juego pedido (todos los users)
        	ResultSet rs = selectStmt.executeQuery("select * from ongames.scores s left join ongames.games g on (s.Games_idGames=g.idGames) left join ongames.users u on (s. Users_idUsers=u.idUsers)where Game_name = '" + game + "' order by Scores desc limit 10"); //order by Scores
            while (rs.next()) {
                scoreRequested = rs.getInt("Scores");
                parsScoreRequested = String.valueOf(scoreRequested);
                scoresArrayList.add(parsScoreRequested);
                gameName = rs.getString("Game_name");
                scoresArrayList.add(gameName);
                userNickname = rs.getString("User_nickname");
                scoresArrayList.add(userNickname);
            }
        } else if(game == null){//SQL que devuelve Scores del user pedido (todos los juegos)
        	ResultSet rs = selectStmt.executeQuery("select * from ongames.scores s left join ongames.games g on (s.Games_idGames=g.idGames) left join ongames.users u on (s. Users_idUsers=u.idUsers)where User_nickname = '" + user + "' order by Scores desc limit 10");
            while (rs.next()) {
                scoreRequested = rs.getInt("Scores");
                parsScoreRequested = String.valueOf(scoreRequested);
                scoresArrayList.add(parsScoreRequested);
                gameName = rs.getString("Game_name");
                scoresArrayList.add(gameName);
                userNickname = rs.getString("User_nickname");
                scoresArrayList.add(userNickname);
            }
        } else {
        	ResultSet rs = selectStmt.executeQuery("select * from ongames.scores s left join ongames.games g on (s.Games_idGames=g.idGames) left join ongames.users u on (s. Users_idUsers=u.idUsers)where Game_name = '" + game + "' and User_nickname = '" + user + "' order by Scores desc limit 10");
            while (rs.next()) {
                scoreRequested = rs.getInt("Scores");
                parsScoreRequested = String.valueOf(scoreRequested);
                scoresArrayList.add(parsScoreRequested);
                gameName = rs.getString("Game_name");
                scoresArrayList.add(gameName);
                userNickname = rs.getString("User_nickname");
                scoresArrayList.add(userNickname);
            }	
        }
      } 
      catch (Exception e) {
        e.printStackTrace();
      }finally {
        try {
          selectStmt.close();
          connection.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
        }
      PrintWriter out = response.getWriter();
      response.setContentType("application/json");
      response.setCharacterEncoding("UTF-8");
      
      
      out.print("{");
      out.print("\"scores\":");
      out.print("[");
      for (int i = 0; i < (scoresArrayList.size()/3); i++){
    	  	out.print("{");
    	    out.print("\"score\":\""+scoresArrayList.get(i*3)+"\"");    
    	    out.print(",");    	    
    	    out.print("\"game\":\""+scoresArrayList.get((i*3)+1)+"\"");
    	    out.print(",");
			out.print("\"user\":\""+scoresArrayList.get((i*3)+2)+"\"");
			out.print("}");
			if (i < ((scoresArrayList.size()/3) - 1)) {
				out.print(",");
			}
    	}
      out.print("]");
      out.print("}");
      out.flush();
      out.close();
      /*{score:21837,user:pepe,game:trashpickup},{score:21837,user:pepe,game:trashpickup},{score:21837,user:pepe,game:trashpickup},
      out.print("{");
      out.print(scoresArrayList);
      out.print("\"}");
      out.flush(); */ 
}

@Override
protected void doPost(
  HttpServletRequest request,
  HttpServletResponse response) throws IOException {
    
    String user = request.getParameter("user");
    String game = request.getParameter("game");
    String score = request.getParameter("score");
    Integer parsScore = Integer.parseInt(score);
    System.out.println(user+" : "+game+" : "+score);      
   
  
      Connection connection = null;
      Statement selectStmt = null;
      String s;
      try{
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ongames", "root", "A2s6wlYFz0Rt7C7Z");
        selectStmt = connection.createStatement();
        ResultSet rs = selectStmt.executeQuery("SELECT idUsers FROM ongames.users where User_nickname = '" + user + "'");
        System.out.println("1 " + rs);
        while (rs.next()) {idUser = rs.getInt("idUsers");}
        System.out.println("2 " + rs);
        ResultSet rs2 = selectStmt.executeQuery("SELECT idGames FROM ongames.games where Game_name = '" + game + "'");
        System.out.println("3 " + rs2);
        while (rs2.next()) {idGame = rs2.getInt("idGames");}
        System.out.println("4 " + rs2);
        selectStmt.executeUpdate("INSERT INTO ongames.scores (Scores, Users_idUsers, Games_idGames) values (" + parsScore + ", " + idUser + ", " + idGame + ")");
      
      } 
      catch (Exception e) {
        e.printStackTrace();
      }finally {
        try {
          selectStmt.close();
          connection.close();
        } catch (Exception e) {
          e.printStackTrace();
        }}
     
  }
 
}